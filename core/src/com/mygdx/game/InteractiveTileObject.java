package com.mygdx.game;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;

public abstract class InteractiveTileObject {

    World world;
    TiledMap map;
    TiledMapTile tile;
    Rectangle bounds;
    Body body;
    Fixture fixture;

    public InteractiveTileObject(World world,TiledMap map,Rectangle bounds){
        this.world = world;
        this.map = map;
        this.bounds = bounds;

        BodyDef bdef = new BodyDef();
        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();

        bdef.type = BodyDef.BodyType.StaticBody;
        bdef.position.set((bounds.getX()+bounds.getWidth()/2)/MyGdxGame.PPM,(bounds.getY()+bounds.getHeight()/2)/MyGdxGame.PPM);
        body = world.createBody(bdef);
        shape.setAsBox(bounds.getWidth()/2/MyGdxGame.PPM, bounds.getHeight()/2/MyGdxGame.PPM);
        fdef.shape = shape;
       fixture = body.createFixture(fdef);

    }
    public abstract void onHit();

    public TiledMapTileLayer.Cell getBlocco(){
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);
        return layer.getCell( (int)(body.getPosition().x/16*MyGdxGame.PPM ), (int) (body.getPosition().y/16*MyGdxGame.PPM));
    }
}
