package com.mygdx.game;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class Bandierina extends InteractiveTileObject {
    ErLucertola erLucertolaa;

    public Bandierina(World world, TiledMap map, Rectangle bounds,ErLucertola erLucertola){

        super(world,map,bounds);
        fixture.setUserData("bandiera");

        erLucertolaa = erLucertola;
    }

    @Override
    public void onHit()
    {

    }

}
