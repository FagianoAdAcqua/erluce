package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class Hud {

    Stage stage;
    Viewport viewport;

   static int score;
   int time = 300;
   Label timelabel;
   float timecount;
   Label viteLabel;
   static int nvite = 5;
   String vite = "x0";

   Label scorelabel;

   Label qualcosa;

    public Hud(SpriteBatch sb){
        score = 0;
        viewport = new FitViewport(400    ,240, new OrthographicCamera());
        stage = new Stage(viewport,sb);
        Table table = new Table();
        table.top();
        table.setFillParent(true);
        scorelabel = new Label(String.format("%06d",score),new Label.LabelStyle(new BitmapFont(),Color.GREEN ));
        viteLabel = new Label(String.format(vite+nvite),new Label.LabelStyle(new BitmapFont(),Color.GREEN));
        timelabel = new Label(String.format("%03d",time),new Label.LabelStyle(new BitmapFont(),Color.GREEN ));
        qualcosa = new Label("QUALCOSA",new Label.LabelStyle(new BitmapFont(),Color.GREEN ));
        table.add(scorelabel).expandX().padTop(10);
        table.add(viteLabel).expandX().padTop(10);
        table.add(timelabel).expandX().padTop(10);
        table.add(qualcosa).expandX().padTop(10);
        stage.addActor(table);

    }
    public void update(float dt){
        scorelabel.setText(String.format("%06d",score));
        viteLabel.setText(String.format(vite+nvite));
        timecount += dt;
        if (timecount>=1){
            time--;
             timelabel.setText(String.format("%03d",time));
             timecount = 0;
        }


    }
    public static void addscore(int value){
        score += value;
    }

}



