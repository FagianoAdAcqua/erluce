package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyGdxGame extends Game {
	SpriteBatch batch;
	PlayScreen playScreen;
	public static final float PPM = 100;
	static AssetManager manager;

	@Override
	public void create () {
		batch = new SpriteBatch();
		manager = new AssetManager();
		manager.load("mario_music.ogg",Music.class);
		manager.load("spacca.wav",Sound.class);
		manager.load("tacchini.wav",Sound.class);
		manager.load("figah.wav",Sound.class); //Non ho l'audio GIGA
		manager.finishLoading();
		playScreen = new PlayScreen(batch);

		setScreen(playScreen);

	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}

