package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Enemy extends Sprite {

    World world;
    Body body;
    PlayScreen playScreen;
    float maxVelocity = 2;


    public Enemy(PlayScreen playScreen,World world, Texture texture, float width, float height, float xposition,float yposition)
    {
        super(texture);
        this.playScreen = playScreen;
        this.world = world;
        setSize(width, height);
        crea(xposition, yposition);
    }

    public void crea(float x, float y)
    {

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x,y);
        body = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(10/MyGdxGame.PPM);
        fixtureDef.shape = circleShape;
        fixtureDef.friction = 0;
        body.createFixture(fixtureDef).setUserData("tacchino");
        EdgeShape testatacc = new EdgeShape();
        testatacc.set(new Vector2(-4/MyGdxGame.PPM,11/MyGdxGame.PPM),new Vector2(4/MyGdxGame.PPM,11/MyGdxGame.PPM));
        fixtureDef.shape = testatacc;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);
        body.setLinearVelocity(-1* maxVelocity,0);
    }
    public void update()
    {
        setPosition(body.getPosition().x-getWidth()/2,body.getPosition().y-getHeight()/2);
        //if (body.getPosition().y)
        if(body.getLinearVelocity().x>=0)
            body.setLinearVelocity(maxVelocity,body.getLinearVelocity().y);
        else if(body.getLinearVelocity().x<0)
            body.setLinearVelocity(-1*maxVelocity,body.getLinearVelocity().y);

    }

    public void kill()
    {
        playScreen.bodiesToRemove.add(body);
        playScreen.enemies.remove(this);

    }
    public void salta(){
        body.applyLinearImpulse(0,5,body.getWorldCenter().x,body.getWorldCenter().y,true);
    }



}
