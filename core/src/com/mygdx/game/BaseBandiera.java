package com.mygdx.game;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class BaseBandiera extends InteractiveTileObject {
    ErLucertola erLucertolaa;
    public BaseBandiera(World world, TiledMap map, Rectangle bounds,ErLucertola erLucertola){
        super(world,map,bounds);
        fixture.setUserData("basebandiera");
        erLucertolaa=erLucertola;
    }

    @Override
    public void onHit() {


    }
}
