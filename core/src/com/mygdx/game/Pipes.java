package com.mygdx.game;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;

public class Pipes extends InteractiveTileObject {

    public Pipes(World world, TiledMap map, Rectangle bounds){

        super(world,map,bounds);
        fixture.setUserData(this);
    }

    @Override
    public void onHit() {

    }
}
