package com.mygdx.game;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class WorldContactListener implements ContactListener {
   int yboyBounces = 0;



    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();


        if (fixA.getUserData()=="Yboypiedi"|| fixB.getUserData() == "Yboypiedi"){
            Fixture yboypiedi = fixA.getUserData() == "Yboypiedi" ? fixA : fixB;
            Fixture fixture = yboypiedi == fixA ? fixB : fixA;

           if (fixture.getUserData() == "ground" || InteractiveTileObject.class.isAssignableFrom(fixture.getUserData().getClass())|| fixture.getUserData()=="intground" || fixture.getUserData() == "tacchino") {
               yboypiedi.getBody().setLinearVelocity(yboypiedi.getBody().getLinearVelocity().x, 2);
               Yboy yboy = (Yboy) yboypiedi.getBody().getFixtureList().get(0).getUserData();
               yboy.yboyBounces++;
               if (yboy.yboyBounces >= 5) {
                   yboy.yboyBounces = 0;
                   yboy.destroy();
               }
           }
           }
        if (fixA.getBody().getFixtureList().get(0).getUserData() instanceof Yboy ||
                fixB.getBody().getFixtureList().get(0).getUserData() instanceof Yboy) {
            Fixture yboy = fixA.getBody().getFixtureList().get(0).getUserData() instanceof Yboy ? fixA : fixB;
            Fixture fixture = yboy == fixA ? fixB : fixA;
            Yboy yboyy = (Yboy) yboy.getBody().getFixtureList().get(0).getUserData();
            if (fixture.getUserData() != null && (Pipes.class.isAssignableFrom(fixture.getUserData().getClass())) || fixture.getUserData() == "intground") {
                yboy.getBody().setLinearVelocity(yboy.getBody().getLinearVelocity().x* - 1, yboy.getBody().getLinearVelocity().y);
                yboyBounces++;
                }
            if (fixture.getUserData() != null && (Enemy.class.isAssignableFrom(fixture.getUserData().getClass()))){
                Enemy enemy = (Enemy) fixture.getBody().getFixtureList().get(1).getUserData();
                yboyy.destroy();
                enemy.kill();
            }
        }


        if (fixA.getUserData() == "testa" || fixB.getUserData()== "testa"){
            Fixture head = fixA.getUserData() == "testa" ? fixA : fixB;
            Fixture blocco = head == fixA ? fixB : fixA;

            if (blocco.getUserData()!= null && Bricketti.class.isAssignableFrom(blocco.getUserData().getClass())) {
                ((Bricketti) blocco.getUserData()).getBlocco().setTile(null);
                PlayScreen.bodiesToRemove.add(blocco.getBody());
                MyGdxGame.manager.get("spacca.wav",Sound.class).play();
            }
            if (blocco.getUserData() != null && Coins.class.isAssignableFrom(blocco.getUserData().getClass())){
                ((Coins) blocco.getUserData()).onHit();

            }

        }

        if (fixA.getUserData() == "erCorpo" || fixB.getUserData() == "erCorpo"){
            Fixture erCorpo = fixA.getUserData() == "erCorpo" ? fixA : fixB;
            Fixture objetc = erCorpo == fixA ? fixB : fixA;

            if (objetc.getUserData() == "basebandiera")
                erCorpo.getBody().setLinearVelocity(3,0);

        }

        if(fixA.getUserData() == "piedi" || fixB.getUserData() == "piedi"){
            Fixture piedi = fixA.getUserData() == "piedi" ? fixA : fixB;
          Fixture objetc = piedi == fixA ? fixB : fixA;

            if (objetc.getUserData()=="bandiera") {
                piedi.getBody().setLinearVelocity(0, -1);


            }



            if (objetc.getBody().getFixtureList().size>1 && objetc.getBody().getFixtureList().get(1).getUserData() instanceof Enemy)
            {

                Enemy tacchino = (Enemy) objetc.getBody().getFixtureList().get(1).getUserData();
                tacchino.kill();
                piedi.getBody().applyLinearImpulse(0,6,piedi.getBody().getWorldCenter().x,piedi.getBody().getWorldCenter().y,true);
                Hud.addscore(50);

                    MyGdxGame.manager.get("tacchini.wav", Sound.class).play();


            }





        }

        else if (fixA.getUserData() == "tacchino" || fixB.getUserData() == "tacchino")
        {
            Fixture tacchino = fixA.getUserData() == "tacchino" ? fixA : fixB;
            Fixture oggetto = tacchino == fixA ? fixB : fixA;

            if(oggetto.getUserData() != null && (Pipes.class.isAssignableFrom(oggetto.getUserData().getClass())|| oggetto.getUserData()=="intground" || oggetto.getUserData() == "tacchino")){
                Enemy tacchinoo = (Enemy) tacchino.getBody().getFixtureList().get(1).getUserData();
                tacchino.getBody().setLinearVelocity(tacchino.getBody().getLinearVelocity().x* (-1),0);

                tacchinoo.flip(true,false);
                    if (oggetto.getUserData()=="tacchino"){
                        Enemy oggettoo = (Enemy) oggetto.getBody().getFixtureList().get(1).getUserData();
                        oggettoo.flip(true,false);
                        oggetto.getBody().setLinearVelocity(oggetto.getBody().getLinearVelocity().x*(-1),0);
                    }

            }



        }

        }




    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
