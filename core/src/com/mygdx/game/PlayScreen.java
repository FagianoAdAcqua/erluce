package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.awt.geom.RectangularShape;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class PlayScreen implements Screen
{
    int yboydirection;
    OrthographicCamera gamecam;
    FitViewport viewport;
    SpriteBatch batch;
    World world;
    ErLucertola erLucertola;
    Texture lucertolaTexture;
    Texture arianTexture;
    Box2DDebugRenderer box2DDebugRenderer;
    TmxMapLoader maploader;
    TiledMap map;
    OrthogonalTiledMapRenderer renderer;
    CreaBlocchi blocchi;
    Texture tacchinotexture;
    float tacchinox;
    float arianx;
    static LinkedList<Body> bodiesToRemove;
    LinkedList<Enemy> enemies;
    LinkedList<Yboy> yboys;
    int nTacchini = 30;
    Hud hud;
    Music music;
    int nArian = 20;
    TiledMap map2;
    Array<Body> mapToremovebodies;
    boolean cambiamappa = true;
    Texture yboyTexture;
    Texture tesxture;
    int nTesta = 30;
    float testx = 40;
    Texture danieletexture;
    float danielex = 30;
    int nDaniele = 30;

    public PlayScreen(SpriteBatch batch)
    {
        this.batch = batch;
        bodiesToRemove = new LinkedList<Body>();
        gamecam = new OrthographicCamera();
        viewport = new FitViewport(400 / MyGdxGame.PPM, 240 / MyGdxGame.PPM, gamecam); //Ora è più zoommato
        gamecam.position.set(100 / MyGdxGame.PPM, viewport.getWorldHeight() / 2, 0); //Ora la visuale è più centrata
        world = new World(new Vector2(0, -10), true);
        box2DDebugRenderer = new Box2DDebugRenderer();
        lucertolaTexture = new Texture("okoke.psd");
        arianTexture = new Texture("arian.psd");
        tesxture = new Texture("fagiano.psd");
        erLucertola = new ErLucertola(this, world, lucertolaTexture, 25 / MyGdxGame.PPM, 25 / MyGdxGame.PPM, 400 / MyGdxGame.PPM, 400 / MyGdxGame.PPM); // Con 1 l'immagine non si vedeva
        maploader = new TmxMapLoader();
        map = maploader.load("level1.tmx");
        map2 = maploader.load("level2.tmx"); // Non ho level2
        renderer = new OrthogonalTiledMapRenderer(map, 1 / MyGdxGame.PPM);
        blocchi = new CreaBlocchi(world, map, erLucertola);
        tacchinotexture = new Texture("download.psd");
        danieletexture = new Texture("Screenshot daniele.psd");
        world.setContactListener(new WorldContactListener());
        enemies = new LinkedList<Enemy>();
        yboys = new LinkedList<Yboy>();
        creanemici();
        hud = new Hud(batch);
        music = MyGdxGame.manager.get("mario_music.ogg", Music.class);
        music.setLooping(true);
        music.play();
        yboyTexture = new Texture("Yboy.psd");
    }


    public void update(float dt)
    {
        handleInput();
        if (erLucertola.body.getPosition().x > 33 && cambiamappa)
            changemap();
        removeBodies(); //I bodies devono essere sempre rimossi prima di chiamare world.step
        world.step(1f / 60f, 6, 2);
        gamecam.position.x = erLucertola.body.getPosition().x; // La y in mario non si aggiorna (se salta la visuale non si sposta)
        gamecam.update();
        hud.update(dt);

        erLucertola.update();



        for (int i = 0; i < yboys.size(); i++)
        {
            yboys.get(i).update();

        }
        for (int i = 0; i < enemies.size(); i++)
        {
            if (enemies.get(i) != null)
                enemies.get(i).update();
        }
        for (int i = 0; i < enemies.size(); i++)
        {// far saltare i tacchini
            if (enemies.get(i).body.getPosition().y <= 0.265 && enemies.get(i).body.getLinearVelocity().y < 0)
                enemies.get(i).salta();
        }
        if (erLucertola.body.getLinearVelocity().x>=0)
            yboydirection = 1;
        else yboydirection =-1;


    }

    public void clearMap()
    {
        mapToremovebodies = new Array<Body>();
        world.getBodies(mapToremovebodies);
        for (Body body : mapToremovebodies)
        {
            world.destroyBody(body);
        }
        mapToremovebodies.clear();
        enemies.clear();
    }
    public void changemap(){

            this.map = map2;
            renderer = new OrthogonalTiledMapRenderer(map2, 1 / MyGdxGame.PPM);
            clearMap();
            erLucertola = new ErLucertola(this, world, lucertolaTexture, 25 / MyGdxGame.PPM, 25 / MyGdxGame.PPM, 400 / MyGdxGame.PPM, 400 / MyGdxGame.PPM);
            blocchi = new CreaBlocchi(world, map2, erLucertola);
            creanemici();// non me li crea.-.
            cambiamappa = false;
    }

    public void creanemici()
    {
        arianx=10;
        tacchinox=0;

        for (int i = 0; i < nTacchini; i++)
        {
            enemies.add(new Enemy(this, world, tacchinotexture, 55 / MyGdxGame.PPM, 40 / MyGdxGame.PPM, tacchinox / MyGdxGame.PPM, 200 / MyGdxGame.PPM));
            tacchinox += (120 + new Random().nextInt(2)); // 2 indica che il valore casuale sarà o 0 o 1 o 2 DA CAMBIARE
        }

        for (int i = 0; i < nArian; i++)
        {
            enemies.add(new Enemy(this, world, arianTexture, 26 / MyGdxGame.PPM, 26 / MyGdxGame.PPM, arianx / MyGdxGame.PPM, 200 / MyGdxGame.PPM));
            arianx += (120 + new Random().nextInt(2));
        }
        for (int i=0; i<nTesta; i++){
            enemies.add(new Enemy(this,world,tesxture,60/MyGdxGame.PPM,50 /MyGdxGame.PPM,testx/MyGdxGame.PPM,100/MyGdxGame.PPM));
            testx += (120 + new Random().nextInt(2));
        }
        for(int i=0; i<nDaniele;i++){
            enemies.add(new Enemy(this,world,danieletexture,140/MyGdxGame.PPM,45/MyGdxGame.PPM,danielex/MyGdxGame.PPM,100/MyGdxGame.PPM));
            danielex += (120 + new Random().nextInt(2));
        }
    }

    public void removeBodies()
    {
        for (Body body : bodiesToRemove)
        {
            if (body.getUserData() instanceof ErLucertola)
            {
                erLucertola.addToWorld(400 / MyGdxGame.PPM, 400 / MyGdxGame.PPM);
                Hud.addscore(-100);
            }
            else if(body.getFixtureList().size>0 && body.getFixtureList().get(0).getUserData() instanceof Yboy){
                System.out.println("  "+body.getFixtureList().get(0).getUserData());
                yboys.remove(body.getFixtureList().get(0).getUserData());
            }
            world.destroyBody(body);

        }
        bodiesToRemove.clear();
    }

    public void draw()
    {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();
        batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
        box2DDebugRenderer.render(world, gamecam.combined);
        batch.setProjectionMatrix(gamecam.combined);

        batch.begin();

        erLucertola.draw(batch);
        for (int i = 0; i < yboys.size(); i++)
        {
            yboys.get(i).draw(batch);
        }
        for (int i = 0; i < enemies.size(); i++)
        {
            enemies.get(i).draw(batch);
        }
        renderer.setView(gamecam);
        batch.end();
    }

    public void handleInput()
    {
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP) && erLucertola.body.getLinearVelocity().y == 0)
            erLucertola.body.applyLinearImpulse(new Vector2(0, 4), erLucertola.body.getWorldCenter(), true);
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && erLucertola.body.getLinearVelocity().x < 3)
        {
            erLucertola.body.applyLinearImpulse(new Vector2(0.1f, 0), erLucertola.body.getWorldCenter(), true);
            if (erLucertola.isFlipX())
                erLucertola.flip(true, false);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && erLucertola.body.getLinearVelocity().x > -3)
        {
            erLucertola.body.applyLinearImpulse(new Vector2(-0.1f, 0), erLucertola.body.getWorldCenter(), true);
            if (!erLucertola.isFlipX())
                erLucertola.flip(true, false);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN))
            erLucertola.body.applyLinearImpulse(new Vector2(0, -4), erLucertola.body.getWorldCenter(), true);
        if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            yboys.add(new Yboy(this, world, yboyTexture, 37 / MyGdxGame.PPM, 14 / MyGdxGame.PPM, erLucertola.body.getPosition().x + 10/MyGdxGame.PPM, erLucertola.body.getPosition().y,yboydirection));
        }
    }


    @Override
    public void show()
    {

    }

    @Override
    public void render(float delta)
    {
        update(delta);
        draw();
    }

    @Override
    public void resize(int width, int height)
    {
        viewport.update(width, height);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        map.dispose();
        map2.dispose();
        renderer.dispose();
        world.dispose();
        box2DDebugRenderer.dispose();
    }


}
