package com.mygdx.game;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Coins extends InteractiveTileObject{
    TiledMapTileSet tileSet;
    boolean addscore = true;
    public Coins(World world,TiledMap map,Rectangle bounds) {

        super(world, map, bounds);
        fixture.setUserData(this);
        tileSet = map.getTileSets().getTileSet("tileset_gutter");

    }

    @Override
    public void onHit() {
        getBlocco().setTile(tileSet.getTile(28));
        if(addscore) {
            addscore = false;
            Hud.addscore(200);
            MyGdxGame.manager.get("figah.wav",Sound.class).play();//Non ho GIGA
        }
    }
}



