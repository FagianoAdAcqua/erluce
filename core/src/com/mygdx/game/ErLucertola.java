package com.mygdx.game;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;



public class ErLucertola extends Sprite
{
    private World world;
    public Body body;
    float velocitay;
    PlayScreen playScreen;


    public ErLucertola(PlayScreen playScreen, World world, Texture texture, float width, float height, float xposition, float yposition)
    {
        super(texture);
        this.playScreen = playScreen;
        this.world = world;
        setSize(width, height);
        addToWorld(xposition, yposition);
    }

    public void addToWorld(Float x, Float y)
    {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        body = world.createBody(bodyDef);
        body.setUserData(this);
        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(10 / MyGdxGame.PPM);
        fixtureDef.shape = circleShape;
        body.createFixture(fixtureDef).setUserData("erCorpo");
        velocitay = body.getLinearVelocity().y;
        EdgeShape piedi = new EdgeShape();
        piedi.set(new Vector2(-4/MyGdxGame.PPM,-11/MyGdxGame.PPM),new Vector2(4/MyGdxGame.PPM,-11/MyGdxGame.PPM));
        fixtureDef.shape = piedi;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData("piedi");
        EdgeShape head = new EdgeShape();
        head.set(-4/MyGdxGame.PPM,11/MyGdxGame.PPM,4/MyGdxGame.PPM,11/MyGdxGame.PPM);
        fixtureDef.shape = head;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData("testa");
    }

    public void update()
    {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
        if (body.getPosition().y <0){
            playScreen.bodiesToRemove.add(body);
            Hud.nvite--;
            }
        if (body.getPosition().x>33){
            playScreen.bodiesToRemove.add(body);
            }
        }
    }






