package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Yboy extends Sprite
{

    World world;
    Body body;
    PlayScreen playScreen;
    int maxVelocity = 3;
    int yboyBounces=0;

    public Yboy(PlayScreen playScreen, World world, Texture texture, float width, float height, float xposition, float yposition, int direction)
    {
        super(texture);
        this.playScreen = playScreen;
        this.world = world;
        setSize(width, height);
        crea(xposition, yposition, direction);
    }

    public void crea(float x, float y, int direction)
    {

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        body = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(5 / MyGdxGame.PPM);
        fixtureDef.shape = circleShape;
        fixtureDef.friction = 0;
        body.createFixture(fixtureDef).setUserData(this);
        body.setLinearVelocity(direction * maxVelocity, 0);
        EdgeShape piedi = new EdgeShape();
        piedi.set(new Vector2(-2 / MyGdxGame.PPM, -6 / MyGdxGame.PPM), new Vector2(2 / MyGdxGame.PPM, -6 / MyGdxGame.PPM));
        fixtureDef.shape = piedi;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData("Yboypiedi");
    }

    public void update()
    {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
        //if (body.getPosition().y)
        if (body.getLinearVelocity().x >= 0)
            body.setLinearVelocity(maxVelocity, body.getLinearVelocity().y);
        else if (body.getLinearVelocity().x < 0)
            body.setLinearVelocity(-1 * maxVelocity, body.getLinearVelocity().y);


    }

    public void destroy()
    {
        playScreen.bodiesToRemove.add(body);
    }

    public void salta()
    {
        body.applyLinearImpulse(0, 2, body.getWorldCenter().x, body.getWorldCenter().y, true);
    }

}
